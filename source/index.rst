.. win2k12-scap documentation master file, created by
   sphinx-quickstart on Fri Jan 12 16:01:49 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Build an SCAP Scanner in Windows Server 2012 R2
===============================================

Contents:

.. toctree::
   :maxdepth: 2

   win2k12-install
   win2k12-config
   install-apps



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

